const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());

let users = [

	{
		username: "tinaRCBC",
		email: "tinaLRCBC@gmail.com",
		password: "tinaRCBC990"
	},
	{
		username: "gwenstacy1",
		email: "spidergwen@gmail.com",
		password: "gwenOGspider"
	}
]

let items = [
	{
		name: "Stick-O",
		price: 150,
		isActive: true
	},
	{
		name: "Doritos",
		price: 150,
		isActive: true
	}
]

app.get('/',(req,res) =>{	

	res.send("Hello World from our first ExpressJS API!")

})

app.get('/hello',(req,res) => {

	res.send("Hello from Batch 152")

})

app.get('/users',(req,res) => {	

	res.send(users)

})

app.post('/users',(req,res) => {

	console.log(req.body);

	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	users.push(newUser);
	console.log(users)

	res.send(users);

})

app.delete('/users',(req,res) => {

	users.pop()
	console.log(users)

	res.send(users)

})

app.put('/users/:index',(req,res) => {

	//req.body will contain the password
	console.log(req.body)
	//req.params object which contain the value of the url params
	//url params is captured by route paramete (:parameterName) and saved as a 
	console.log(req.params)	

	let index = parseInt(req.params.index)

	users[index].password = req.body.password
	res.send(users[index])
/*
	users[req.body.index].password = req.body.password
	res.send(users[req.body.index])
*/

})

//get details of single user
//get method request should not have a request body. It may have headers for additional information. We can pass small amount of data somewhere else: Through the url.
//route params are values we can pass via the URL.
//This is done specifically to allow us to send small amount of data into our server through the request URL.
//route parameters can be defined in the endpoint of a route with :parameterName
app.get('/users/getSingleUser/:index',(req,res) => {

	//http://localhost:4000/users/getSingleUser/0
	//req.params is an object that contains the values passed as route params.
	//in req.params, the parameter name give in the route endpoint becomes the property name
	console.log(req.params);//{index:0}

	//parseInt() the value of req.params.index to convert the value from string to number
	let index = parseInt(req.params.index)
	console.log(index)

	//send the user with the appropriate index number
	res.send(users[index])
	
})

app.get('/items',(req,res) => {

	res.send(items)

})

app.post('/items',(req,res) => {

	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: true
	}

	items.push(newItem)
	console.log(items)
	res.send(items)

})

app.put('/items/:index',(req,res) => {

	let index = parseInt(req.params.index)

	if(req.body.price != null){
		items[index].price = req.body.price	
	}
	if(req.body.name != null){
		items[index].name = req.body.name		
	}
	if(req.body.isActive != null){
		items[index].isActive = req.body.isActive	
	}
	res.send(items[index])
})


app.get('/items/getSingleItem/:index',(req,res) =>{

	let index = parseInt(req.params.index)

	res.send(items[index])
})


app.put('/items/archive/:index',(req,res) =>{

	let index = parseInt(req.params.index)

	items[index].isActive = false
	res.send(items[index])
})

app.put('/items/activate/:index',(req,res) =>{

	let index = parseInt(req.params.index)

	items[index].isActive = true
	res.send(items[index])

})



app.listen(port, () => console.log(`Server is running at port ${port}`));

